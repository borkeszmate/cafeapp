import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../../interfaces/product';
import { environment } from 'src/environments/environment';
import { NavController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { BuyProductPage } from '../../modals/buy-product/buy-product.page';

@Component({
	selector: 'app-details',
	templateUrl: './details.page.html',
	styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
	productId: number;
	product: Product;
	productsLoaded = false;
	apiUrl = environment.http;

	constructor(
		private productsService: ProductsService,
		private route: ActivatedRoute,
		private router: Router,
		public navCtrl: NavController,
		public modalController: ModalController
	) { }

	ngOnInit() {
	}

	ionViewWillEnter() {
		console.log(this.navCtrl);
		this.productId = this.route.snapshot.params.id;
		this.productsService.getProduct(this.productId).subscribe((product: Product[]) => {
			this.product = product[0];
			this.productsLoaded = true;
		});
	}

	navigateBack() {
		this.navCtrl.navigateBack('/products');
	}

	async buyProduct() {
		console.log('fut');
		const modal = await this.modalController.create({
			component: BuyProductPage,
			componentProps: {
				product: this.product
			}
		});
		return await modal.present();
	}
}
