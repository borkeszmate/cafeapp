import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
		{ path: 'products', loadChildren: './home/home.module#HomePageModule' },
		{ path: 'details/:id', loadChildren: './product/details/details.module#DetailsPageModule' },
		{ path: 'about', loadChildren: './about/about.module#AboutPageModule' },
		{ path: 'profile',
			children: [
				{
					path: 'pay',
					loadChildren: './user/pay/pay.module#PayPageModule'
				},
				{
					path: 'details',
					loadChildren: './user/profile/profile.module#ProfilePageModule'
				},
			]
		},

	{ path: 'buy-product', loadChildren: './modals/buy-product/buy-product.module#BuyProductPageModule' },
	{ path: '**', redirectTo: '/products' },
];

@NgModule({
	imports: [
	RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
	],
	exports: [RouterModule]
})
export class AppRoutingModule { }
