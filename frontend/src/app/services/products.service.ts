import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
	providedIn: 'root'
})
export class ProductsService {
	apiUrl = environment.http;
	constructor(
		private http: HttpClient
	) { }

	getProducts() {
		return this.http.get(`${this.apiUrl}/api/products`);
	}

	getProduct(id: number) {
		return this.http.get(`${this.apiUrl}/api/products/${id}`);
	}
}
