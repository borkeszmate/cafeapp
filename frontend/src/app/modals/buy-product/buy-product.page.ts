import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../interfaces/product';

@Component({
	selector: 'app-buy-product',
	templateUrl: './buy-product.page.html',
	styleUrls: ['./buy-product.page.scss'],
})
export class BuyProductPage implements OnInit {
	@Input() product: Product;
	constructor() { }

	ngOnInit() {
		console.log(this.product);
	}

}
