import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../services/products.service';
import { ActionSheetController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Product } from '../interfaces/product';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

	products: Product[] = [];
	productsLoaded = false;
	apiUrl = environment.http;

	constructor(
		public actionSheetController: ActionSheetController,
		private productsService: ProductsService,
		public loadingController: LoadingController
	) {}

	ngOnInit() {

	}

	ionViewWillEnter() {
		this.productsService.getProducts().subscribe((prods: Product[]) => {
			this.products = prods;
			console.log(prods);
			for (const i of this.products) {
				console.log(i);
			}
		});
	}


	// Ionic utility methods
	async presentLoading() {
		const loading = await this.loadingController.create({
			message: 'Hellooo',
		});
		await loading.present();
	}
}
