const express = require('express');
const router = express.Router();

const products = require('../controllers/products');
const user = require('../controllers/user');



// Product routes
router.get('/products', products.getAllProducts);
router.get('/products/:id', products.getSingleProduct);

// User routes

router.get('/user/payment', user.savePayment);

module.exports = router;
