const pool = require('../util/db.js');

exports.getAllProducts = (req, res, next) => {

	pool.query('SELECT * FROM products', (err, rows, fields) => {

		console.log(rows[3].name);
		res.send(rows);
	})
}

exports.getSingleProduct = (req, res, next) => {
	const id = req.params.id;
	console.log(id);

	pool.query('SELECT * FROM products WHERE id = ?', [id], (err, rows, fields) => {
		if (err) {
			console.log(err);
		}
		res.send(rows);
	});
}
