const pool = require('../util/db.js');
const request = require('request');

exports.savePayment = (req, res, next ) => {
	request.post('https://api.barion.com/v2/Payment/Start', {
		json: {
			POSKey: "f620f6b772ac4fc6bce9130511c3e2dc",
			PaymentType: "Immediate",
			PaymentRequestId: "EXMPLSHOP-PM-001",
			FundingSources: ["All"],
			Currency: "EUR",
			Transactions: [
				{
					POSTransactionId: "EXMPLSHOP-PM-001/TR001",
					Payee: "webshop@example.com",
					Total: 37.2,
					Comment: "A brief description of the transaction",
					Items: [
						{
							Name: "iPhone 7 smart case",
							Description: "Durable elegant phone case / matte black",
							Quantity: 1,
							Unit: "piece",
							UnitPrice: 25.2,
							ItemTotal: 25.2,
							SKU: "EXMPLSHOP/SKU/PHC-01"
						},
						{
							Name: "Fast delivery",
							Description: "Next day delivery",
							Quantity: 1,
							Unit: "piece",
							UnitPrice: 12,
							ItemTotal: 12,
							SKU: "EXMPLSHOP/SKU/PHC-01"
						}
					]
				}
			]

		}
	})
	.on('response', (response) => {

		console.log('response', response);
		res.send(response);
	})
	.on('error', (error) => {
		console.log('hiba',error)
		res.send(error);
	});
}
